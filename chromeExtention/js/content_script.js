function getTitle() {
	return document.getElementsByClassName('b-material-new__head-title')[0].textContent;
};
function getFilename() {
	return document.getElementsByClassName('current-file-fullname')[0].textContent;
};
function getTranslation() {
	var translationWrapper = document.getElementsByClassName('b-action m-sound m-arrowed')[0];
	return translationWrapper.children[1].children[0].textContent;
};
function getTime() {
	// var time = prompt('Введіть час, на якому завершили перегляд:');
	// if (time && time.length > 0)
	// 	return time;
	// else
	// 	return null;
	return "00:00";
};
function getUrl() {
	return window.location.href;
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  	if (request.type && (request.type === "ADD_NOTE")) {
  		var time = getTime();

  		if(time)
  			chrome.runtime.sendMessage({ 
				type: "NOTE_DATA", 
				title: getTitle(), 
				filename: getFilename(),
				url: getUrl(),
				translation: getTranslation(),
				stopTime: time
			});
    	else
    		alert("Ви не ввели час.");
  	}
});
