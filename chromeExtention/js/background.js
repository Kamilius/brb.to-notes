var notesList = [];

function setChromeLocalStorage() {
  localStorage.setItem('brbToNotes', JSON.stringify(notesList));
};

function getChromeLocalStorage () {
  	var localStorageNotes = localStorage.getItem('brbToNotes');
  	if (localStorageNotes)
    	return JSON.parse(localStorageNotes);
	else
		return null;
}

function getNoteByTitle (title) {
  for(var i = 0; i < notesList.length; i++)
    if(notesList[i].title === title)
      return i;
  return -1;
};

function editAddNote (data) {
	var noteIndex = getNoteByTitle(data.title);
  	if(noteIndex >= 0) {
   	 	notesList[noteIndex].title = data.title;
    	notesList[noteIndex].filename = data.filename;
    	notesList[noteIndex].url = data.url;
    	notesList[noteIndex].translation = data.translation;
    	notesList[noteIndex].stopTime = data.stopTime;
  	} else {
  		var newId = 1;

	    if(notesList.length > 0)
	      	newId = notesList[notesList.length - 1].id + 1;

    	notesList[notesList.length] = {
			id: newId,
			title: data.title,
			filename: data.filename,
			url: data.url,
			translation: data.translation,
			stopTime: data.stopTime
		}
		notesList.sort(function(left, right) { 
			return left.title == right.title ? 0 : (left.title < right.title ? -1 : 1);
		});
  	}
};

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if (request.type && (request.type == "NOTE_DATA")) {
  	notesList = getChromeLocalStorage();
  	editAddNote(request);
  	setChromeLocalStorage();

  	chrome.runtime.sendMessage({
  		type: "UPDATE_POPUP_DATA",
  	});
  }
});