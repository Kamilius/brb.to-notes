(function(){
  function LanguageModel() {
  	var self = this;
  	self.currentLang = ko.observable();
  	self.ua = {
  		add: "Додати серію",
  		remove: "Видалити",
  		empty: "Наразі не додано жодної серії."
  	};
  	self.eng = {
  		add: "Add current series",
  		remove: "Remove",
  		empty: "Currently no items added."
  	};
  	self.rus = {
  		add: "Добавить серию",
  		remove: "Удалить",
  		empty: "Пока не добавлено ни одной серии."
  	}
  	self.getLang = ko.computed(function(){
  		switch(self.currentLang()) {
  			case "ua":
  				return self.ua;
  			case "eng":
  				return self.eng;
  			case "rus":
  				return self.rus;
  		}
  	});
  	self.getLocalStorageLang = function() {
  		var lang = localStorage.getItem("brbToLang");
  		if(lang && lang.length > 0)
  			self.currentLang(lang);
  		else
  			self.currentLang("eng");
  	};
  	self.setLocalStorageLang = function(item, element) {
      var lang = element.currentTarget.className.substr(5, 10);
      self.currentLang(lang);
  		localStorage.setItem("brbToLang", lang);
  	};

    self.getLocalStorageLang();
  }

  function Note(id, title, filename, url, translation, stopTime) {
    var self = this;
    self.id = ko.observable(id);
    self.title = ko.observable(title);
    self.filename = ko.observable(filename);
    self.url = ko.observable(url);
    self.translation = ko.observable(translation);
    self.stopTime = ko.observable(stopTime);
  }
  function SeriesNotesViewModel() {
    var self = this;
    self.notesList = ko.observableArray();

    self.languages = new LanguageModel();
    
    self.showList = ko.computed(function(){
      if(self.notesList().length > 0)
        return true;
      else
        return false;
    });

    self.addNote = function() {
      chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { type: "ADD_NOTE" });
      });
    }

    self.removeNote = function(item) {
      self.notesList.remove(item);
      self.setChromeLocalStorage();
    };

    self.setChromeLocalStorage = function() {
      localStorage.setItem('brbToNotes', ko.toJSON(self.notesList()));
    };

    self.getChromeLocalStorage = function() {
      var localStorageNotes = localStorage.getItem('brbToNotes');
      if (localStorageNotes) {
        if(self.notesList().length > 0)
          self.notesList.removeAll();
        var parsedNotes = JSON.parse(localStorageNotes);
        parsedNotes.sort(function(left, right) { 
          return left.title == right.title ? 0 : (left.title < right.title ? -1 : 1);
        });
        for (var i = 0; i < parsedNotes.length; i++)
          self.notesList.push(new Note(parsedNotes[i].id, parsedNotes[i].title, parsedNotes[i].filename, parsedNotes[i].url, parsedNotes[i].translation, parsedNotes[i].stopTime));
      }
    }

    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
      if (request.type && (request.type == "UPDATE_POPUP_DATA"))
        self.getChromeLocalStorage();
    });

    self.getChromeLocalStorage();
  };
  var options = {
   attribute: "data-bind",        // default "data-sbind"
   globals: window,               // default {}
   bindings: ko.bindingHandlers,  // default ko.bindingHandlers
   noVirtualElements: false       // default true
  };
  ko.bindingProvider.instance = new ko.secureBindingsProvider(options);
  ko.applyBindings(new SeriesNotesViewModel());
})();